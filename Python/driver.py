from account import Account

class Driver(Account):
    def __init__(self, _name, _document, _email, _password):
        super().__init__(_name, _document)
        self.email = _email
        self.password = _password