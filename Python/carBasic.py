from car import Car

class CarBasic(Car):
    brand = str
    model = str

    def __init__(self, _license, _driver, _brand, _model):
        super().__init__(_license, _driver)
        self.brand = _brand
        self.model = _model