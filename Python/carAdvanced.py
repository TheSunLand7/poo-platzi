from car import Car

class CarAdvanced(Car):
    typeCarAccepted = []
    seatMaterial = []

    def __init__(self, _license, _driver, _typeCarAccepted, _seatMaterial):
        super().__init__(_license, _driver)
        self.typeCarAccepted = _typeCarAccepted
        self.seatMaterial = _seatMaterial