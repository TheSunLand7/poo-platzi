<?php
require_once 'Account.php';
class Car {
    public $id;
    public $license;
    public $driver;
    public $passenger;

    public function __construct($_license, Account $_driver) {
        $this->license = $_license;
        $this->driver = $_driver;
    }

    public function printDataCar()
    {
        echo "La licencia es: ".$this->license.", el driver es: ".$this->driver->name.", y su documento es: ".$this->driver->document;
    }
}

?>