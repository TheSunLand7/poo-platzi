<?php 
    require_once 'Car.php';
    class CarAdvanced extends Car {
        public $typeCarAccepted;
        public $seatMaterial;

        public function __construct($_license, $_driver, $_typeCarAccepted, $_seatMaterial) {
            parent::__construct($_license, $_driver);
            $this->typeCarAccepted = $_typeCarAccepted;
            $this->seatMaterial = $_seatMaterial;
        }
    }
    
?>