<?php
    require_once 'Car.php';
    class CarBasic extends Car {
        public $brand;
        public $model;

        public function __construct($_license, $_driver, $_brand, $_model) {
            parent::__construct($_license, $_driver);
            $this->brand = $_brand;
            $this->model = $_model;
        }
    }
    
?>