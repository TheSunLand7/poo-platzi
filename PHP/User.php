<?php 
    require_once 'Account.php';
    class User extends Account {
        public function __construct($_name, $_document, $_email, $_password) {
            parent::__construct($_name, $_document);
            $this->email = $_email;
            $this->password = $_password;
        }
    }
?>