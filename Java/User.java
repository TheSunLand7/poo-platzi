/**
 * User
 */

class User extends Account {
    public User(String _name, String _document, String _email, String _password) {
        super(_name, _document);
        this.email = _email;
        this.password = _password;
    }
}