class Main {
    public static void main(String[] args) {
        UberX uberX = new UberX("PER123", new Account("Omar Telos", "OMA123"), "Tesla", "Y");
        uberX.setPassenger(4);
        uberX.printDataCar();

        CarAdvanced car = new CarAdvanced("COR345", new Account("Raz Mantz", "RAZ342"));
        car.setPassenger(5);
        car.printDataCar();
    }
}