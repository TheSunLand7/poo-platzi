import java.util.ArrayList;
import java.util.Map;

class CarAdvanced extends Car {
    Map<String, Map<String, Integer>> typeCarAccepted;
    ArrayList<String> seatMaterial;
    
    public CarAdvanced(String _license, Account _driver,  Map<String, Map<String, Integer>> _typeCarAccepted, ArrayList<String> _seatMaterial) {
        super(_license, _driver);
        this.typeCarAccepted = _typeCarAccepted;
        this.seatMaterial = _seatMaterial;
    }
    public CarAdvanced(String _license, Account _driver) {
        super(_license, _driver);
    }

    @Override //Aplicando polimorfismo (sobreescribiendo el setter)
    public void setPassenger(Integer _passenger) {
        // TODO Auto-generated method stub
        if (_passenger == 6) {
            this.passenger = _passenger;
        } else {
            System.out.println("Error Van: Only 6 passengers are allowed!");
        }
    }
}