
/**
 * Car
 */
class Car {
    private Integer id;
    private String license;
    private Account driver; // Objeto de tipo 'Driver'
    protected Integer passenger;

    public Car(String _license, Account _driver) {
        this.license = _license;
        this.driver = _driver;
    }

    void printDataCar () {
        if (passenger != null) {
            System.out.println("License: " + license +  " | Name Driver: " + driver.name + " | Passengers: " + passenger);
        }
    }

    public Integer getPassenger() {
        return passenger;
    }

    public void setPassenger(Integer _passenger) {
        if (_passenger == 4) {
            this.passenger = _passenger;
        } else {
            System.out.println("Error: Only 4 passengers are allowed!");
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public Account getDriver() {
        return driver;
    }

    public void setDriver(Account driver) {
        this.driver = driver;
    }

    
}