import java.util.ArrayList;
import java.util.Map;

class UberBlack extends CarAdvanced {
    public UberBlack(String _license, Account _driver,  Map<String, Map<String, Integer>> _typeCarAccepted, ArrayList<String> _seatMaterial) {
       super(_license, _driver, _typeCarAccepted, _seatMaterial);
    }
}