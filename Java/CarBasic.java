class CarBasic extends Car {
    String brand;
    String model;
    public CarBasic(String _license, Account _driver, String _model, String _brand) {
        super(_license, _driver);
        this.model = _model;
        this.brand = _brand;
    }
}