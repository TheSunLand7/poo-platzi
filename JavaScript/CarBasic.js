class CarBasic extends Car {
    constructor(_license, _driver, _brand, _model) {
        super(_license, _driver);
        this.brand = _brand;
        this.model = _model;
    }
}