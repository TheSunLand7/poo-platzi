class CarAdvanced extends Car {
    constructor(_license, _driver, _typeCarAccepted, _seatMaterial) {
        super(_license, _driver);
        this.brand = _typeCarAccepted;
        this.model = _seatMaterial;
    }
}